<?php declare(strict_types=1);

/**
 * @file
 * Token integration for the Ecwid module.
 */

use Drupal\Core\Render\BubbleableMetadata;
use Drupal\menu_link_content\Plugin\Menu\MenuLinkContent;

function ecwid_drupal_token_info() {
  $info = [];

  $info['tokens']['site']['ecwid_store_base_path'] = [
    'name' => t('Ecwid store base path'),
    'description' => t('Prepended to every store URL.')
  ];
  $info['tokens']['ecwid_page']['parent_menu_path'] = [
    'name' => t('Parent menu path'),
    'description' => t('The path constructed from parent menu items.'),
  ];

  return $info;
}

/**
 * Implements hook_tokens().
 */
function ecwid_drupal_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];
  $store_base_path = \Drupal::config('ecwid_drupal.settings')->get('store_base_path');

  foreach ($tokens as $name => $original) {
    switch ($name) {

      // Store base path, e.g. '/products'.
      case 'ecwid_store_base_path':
        $replacements[$original] = $store_base_path;
        break;

      // An Ecwid Page entity's parent menu path. Allows SEO friendly and
      // just generally pleasant hierarchical URLs.
      case 'parent_menu_path':
        if ($type !== "ecwid_page" || empty($data['ecwid_page'])) {
          break;
        }

        /** @var \Drupal\Core\Menu\MenuLinkManagerInterface $menu_link_manager */
        $menu_link_manager = \Drupal::service('plugin.manager.menu.link');

        // Get the URL of the Ecwid Page, then get any menu items pointing to
        // that URL.
        $url = $data['ecwid_page']->toUrl();
        $menuLinks = $menu_link_manager->loadLinksByRoute(
          $url->getRouteName(),
          $url->getRouteParameters(),
        );

        // For simplicities sake, use only the first found menu item.
        $menuLink = reset($menuLinks);

        // If that menu item has a parent, get its URL,
        if ($menuLink && $parentLinkId = $menuLink->getParent()) {
          $parentLink = $menu_link_manager->createInstance($parentLinkId);
          if ($parentLink instanceof MenuLinkContent) {
            $path = explode('/', trim($parentLink->getUrlObject()->toString(), "/"));
            // ... minus the store's base path (that's a different URL segment
            // and token).
            if ('/' . $path[0] === $store_base_path) {
              $path = array_slice($path, 1);
            }
          }
        }

        // For consistency, this always starts with a "/".
        $replacements[$original] = "/" . implode('/', $path ?? []);

        break;
    }
  }

  return $replacements;
}
