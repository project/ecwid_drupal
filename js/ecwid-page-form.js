(function (Drupal) {
  Drupal.behaviors.ecwidPageForm = {
    attach: function (context, settings) {
      settings.ecwid_drupal = settings.ecwid_drupal || {
        titleListenerAdded: '',
        userEditedTitle: false,
      };

      const titleField = context.querySelector('input[name="title[0][value]"]');

      if (
        titleField &&
        settings.ecwid_drupal.titleListenerAdded !== titleField.id
      ) {
        titleField.addEventListener('input', function () {
          const categoryField = document.querySelector('#edit-ecwid-category');
          const selectedCategory =
            categoryField.options[categoryField.selectedIndex].text;

          if (selectedCategory !== titleField.value) {
            settings.ecwid_drupal.userEditedTitle = true;
          } else {
            settings.ecwid_drupal.userEditedTitle = false;
          }
        });
        settings.ecwid_drupal.titleListenerAdded = titleField.id;
      }

      const categoryField = context.querySelector('#edit-ecwid-category');
      if (
        categoryField &&
        categoryField.selectedIndex > 0 &&
        !settings.ecwid_drupal.userEditedTitle
      ) {
        const selectedCategory =
          categoryField.options[categoryField.selectedIndex].text;
        if (titleField) {
          titleField.value = selectedCategory;
        }
      }
    },
  };
})(Drupal);
