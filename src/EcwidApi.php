<?php declare(strict_types=1);

namespace Drupal\ecwid_drupal;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\ecwid_drupal\Exception\EcwidApiError;
use Drupal\ecwid_drupal\Exception\InvalidPathException;
use Drupal\ecwid_drupal\Exception\MissingStoreId;
use Drupal\ecwid_drupal\Exception\TypeError;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Utils;
use Psr\Http\Message\ResponseInterface;

/**
 * @template C
 * @template R
 */
final readonly class WithCache {
  private function __construct(
    public readonly CacheValue|CacheClear $cache,
    public readonly mixed $return,
  ) {}

  /**
   * @param CacheValue<C>|CacheClear $cache
   * @param R $return
   * @return WithCache<C, R>
   */
  public static function create(CacheValue|CacheClear $cache, mixed $return) {
    return new static($cache, $return);
  }
}

/**
 * @template T
 */
final readonly class CacheValue {
  private function __construct(
    public readonly mixed $value,
  ) {}

  /**
   * @param T $value
   * @return CacheValue<T>
   */
  public static function create($value) {
    return new static($value);
  }
};

final readonly class CacheClear { };

/**
 * Provides a service for interacting with the Ecwid API.
 */
class EcwidApi {
  use StringTranslationTrait;

  /**
   * Id of the Ecwid store, for building requests.
   */
  protected string $store_id;

  protected function getDefaultDecoder(): \Closure {
    return fn(ResponseInterface|\stdClass $response): WithCache => (fn(
      array $decoded
    ): WithCache => WithCache::create(cache: CacheValue::create($decoded), return: $decoded))(
      match (TRUE) {
        // A ResponseInterface is fresh from the EcwidApi.
        $response instanceof ResponseInterface => $this->jsonDecode(
          $response->getBody()->getContents()
        ),
        // \stdClass means the value came from the cache.
        $response instanceof \stdClass && !empty ($response->data)
        => $response->data,
      }
    );
  }

  /**
   * Throws an error or continues execution in a way that allows chaining.
   *
   * Useful for functions that assume a store id is set.
   */
  protected function ensureStoreId(): EcwidApi {
    if (empty($this->store_id)) {
      throw new MissingStoreId("Ecwid API error: store id not set");
    }
    else {
      return $this;
    }
  }

  protected function apiUrl(string $path): string {
    if (strpos($path, "/") === 0) {
      return "https://app.ecwid.com/api/v1/$this->store_id" .
        $path .
        (str_contains($path, "?") ? "&" : "?") .
        "cleanUrls=true";
    }
    else {
      throw new InvalidPathException(
        "Path: '$path' did not begin with a '/' character"
      );
    }
  }

  /**
   * Constructs a new EcwidApi object.
   */
  public function __construct(
    protected readonly ClientInterface $httpClient,
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly CacheBackendInterface $cache,
    protected readonly LoggerChannelFactoryInterface $loggerFactory,
    // Translation is declared in `StringTranslationTrait`.
    TranslationInterface $stringTranslation
  ) {
    $store_id = $configFactory->get("ecwid_drupal.settings")->get("store_id");
    $this->store_id = $store_id ?? "";
    $this->stringTranslation = $stringTranslation;
  }

  /**
   * Decode JSON from Ecwid and narrow type of result to an array.
   *
   * Throws a type error if the decoded result is not an array.
   */
  protected function jsonDecode(string $json): array {
    $result = Utils::jsonDecode($json, TRUE);

    if (is_array($result)) {
      return $result;
    }
    else {
      throw new TypeError(
        "Error decoding response from Ecwid, the result is not an array."
      );
    }
  }

  protected function withCategoryCaching(
    \Closure $fn,
    int|null $id = NULL
  ): array {
    $storeId = $this->store_id;
    $cacheId = "ecwid:$storeId:category:" . ($id ?? "");
    $indexCacheId = "ecwid:$storeId:category_index";
    $indexCache = ($this->cache->get($indexCacheId) ?: NULL)?->data ?: [];
    $categoryTags = ["ecwid:$storeId:category"];

    if (!is_null($id)) {
      $category =
        ($this->cache->get($cacheId) ?: NULL)?->data ?:
        $this->cache->set(
          $cacheId,
          $fn($this->ensureStoreId()->httpClient, $id),
          REQUEST_TIME + 3600 * 24,
          $categoryTags
        ) ?:
        ($this->cache->get($cacheId) ?: NULL)?->data;

      if (!empty($indexCache) && empty($indexCache[$cacheId])) {
        // The index cache does not contain the category, so it must be
        // outdated, clear it.
        $this->cache->delete($indexCacheId);
      }

      return $category;
    }
    elseif (is_null($id) && empty($indexCache)) {
      $categories = $fn($this->ensureStoreId()->httpClient);

      $this->cache->set(
        $indexCacheId,
        array_reduce(
          $categories,
          fn(array $categoryIndex, array $category) => [
            ...$categoryIndex,
            $cacheId . $category["id"] => $cacheId . $category["id"],
          ],
          []
        ),
        REQUEST_TIME + 3600 * 24,
        $categoryTags
      );

      $this->cache->setMultiple(
        array_reduce(
          $categories,
          fn(array $cachable, array $category) => [
            $cacheId . $category["id"] => [
              "data" => $category,
              "expire" => REQUEST_TIME + 3600 * 24,
              "tags" => $categoryTags,
            ],
          ],
          []
        )
      );

      return $categories;
    }
    elseif (is_null($id) && !empty($indexCache)) {
      // Note: `getMultiple()` alters `$indexCache`.
      $cached = $this->cache->getMultiple($indexCache);

      if (!empty($indexCache)) {
        // Refresh the complete category cache, rather than making multiple
        // calls to the Ecwid API.
        \Drupal\Core\Cache\Cache::invalidateTags($categoryTags);
        return $this->withCategoryCaching($fn);
      }
      else {
        return $cached;
      }
    }
    else {
      throw new \Exception(
        "`if` statement did not cover every possibility. This is a bug"
      );
    }
  }

  protected function withProductCaching(\Closure $fn, int $id) {
    $cacheId = "ecwid:" . $this->store_id . ":product:" . $id;
    $productTags = ["ecwid:" . $this->store_id . ":product"];
    $product =
      ($this->cache->get($cacheId) ?: NULL)?->data ?:
      $this->cache->set(
        $cacheId,
        $fn($this->ensureStoreId()->httpClient, $id),
        REQUEST_TIME + 3600 * 24,
        $productTags
      ) ?:
      ($this->cache->get($cacheId) ?: NULL)?->data;

    assert(is_array($product, new TypeError("Product must be an array")));

    return $product;
  }

  protected function cacheAndReturn(string $cid, WithCache $value, array $tags = []) {
    if ($value->cache instanceof CacheClear) {
      $this->cache->delete($cid);
    }
    else {
      ($this->cache->get($cid) ?: NULL)?->data === $value->cache->value ?:
        $this->cache->set($cid, $value->cache->value, REQUEST_TIME + 3600 * 24, $tags);
    }

    return $value->return;
  }

  /**
   * Perform a request and cache the result in the profile cache.
   * 
   * @template T
   * 
   * @param \Closure(ClientInterface): ResponseInterface $fn
   *  Function that takes a request and returns a response.
   * 
   * @return \Closure(\Closure((ResponseInterface|\stdClass): WithCache)|null): T
   *  Function that optionally takes a decoder function, and returns a result.
   */
  protected function withProfileCaching(\Closure $fn): \Closure {
    $cacheId = "ecwid:" . $this->store_id . ":profile";

    // Match based on whether the profile is in the cache.
    // Both branches return a function that takes an optional decoder as
    // its parameter.
    return match (($cached = $this->cache->get($cacheId))) {

      // The cache does not contain a valid value.
      FALSE => fn(\Closure|null $decoder = NULL) =>
        
        // This does two things:
        // 1. Sets `$response` to the result of calling `$fn` with the request
        // 2. Matches on the status code of `$response`
        match (
        ($response = $fn($this->httpClient))->getStatusCode()
        ) {
          
          // All is well, the status code is a 200.
          200 => $this->cacheAndReturn(
            $cacheId,

            // Either call the default request decoder, or the user's function.
            empty($decoder)
              ? $this->getDefaultDecoder()($response)
              : $decoder($response)
          ),
          
          // Response has a result code of something other than 200.
          default => throw new EcwidApiError(
            "Error fetching Ecwid profile. Fetching the store profile returned an error: " .
            "{$response->getStatusCode()} {$response->getReasonPhrase()}"
          ),
        },

      // `$cached` is set to something other than `false`.
      default => fn(\Closure|null $decoder = NULL) => $this->cacheAndReturn(
        $cacheId,
        
        // As above, call the default request decoder or user function.
        // Importantly, the decoder has the option of clearing the cache by
        // returning `CacheClear`.
        empty($decoder)
          ? $this->getDefaultDecoder()($cached)
          : $decoder($cached)
      ),
    };
  }

  public function getCategories(): array {
    return $this->withCategoryCaching(
      fn(ClientInterface $httpClient) => $this->jsonDecode(
        $httpClient
          ->request("GET", $this->apiUrl("/categories"))
          ->getBody()
          ->getContents()
      )
    );
  }

  public function getCategory(int $categoryId): array {
    return $this->withCategoryCaching(
      fn(ClientInterface $httpClient, int $categoryId) => $this->jsonDecode(
        $httpClient
          ->request("GET", $this->apiUrl("/category?id={$categoryId}"))
          ->getBody()
          ->getContents()
      ),
      $categoryId
    );
  }

  public function getProduct(int $productId): array {
    return $this->withProductCaching(
      fn(ClientInterface $httpClient) => $this->jsonDecode(
        $httpClient
          ->request("GET", $this->apiUrl("/products?id={$productId}"))
          ->getBody()
          ->getContents()
      ),
      $productId
    );
  }

  /**
   * Get the store profile and return a boolean indicating if it's valid.
   */
  public function validateEcwidStore(string $store_id = NULL): bool {
    // If the store id wasn't passed, use the one stored in config.
    $store_id = $store_id ?? $this->store_id;

    // And if there is no store id at all, return false.
    if (empty($store_id)) {
      return FALSE;
    }

    try {
      return $this->withProfileCaching(
        fn(
        ClientInterface $httpClient
      ): ResponseInterface => $httpClient->request(
          "GET",
          "https://app.ecwid.com/api/v1/$store_id/profile?clean_urls=true"
        )
      )(
        fn(ResponseInterface|\stdClass $response) => (fn(WithCache $processed): WithCache => WithCache::create(
          cache: $processed->cache,
          return: isset($processed->return["storeName"]),
        ))($this->getDefaultDecoder()($response))
      );
    }
    catch (EcwidApiError $e) {
      $this->loggerFactory
        ->get("ecwid")
        ->error(
          $this->t("Validating Ecwid store failed with the following error: ") .
          $e
        );
      return FALSE;
    }
  }

  /**
   * Get the Ecwid store profile.
   *
   * Includes information like the store name.
   */
  public function getStoreProfile(): array {
    return $this->withProfileCaching(
      fn(
      ClientInterface $httpClient
    ): ResponseInterface => $httpClient->request(
        "GET",
        $this->apiUrl("/profile")
      )
    )();
  }
}
