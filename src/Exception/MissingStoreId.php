<?php declare(strict_types=1);

namespace Drupal\ecwid_drupal\Exception;

class MissingStoreId extends EcwidApiError {
}
