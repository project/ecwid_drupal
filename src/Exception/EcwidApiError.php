<?php declare(strict_types=1);

namespace Drupal\ecwid_drupal\Exception;

/**
 * Ecwid API error.
 *
 * Something went wrong when communicating with Ecwid. Also includes JSON
 * decoding errors.
 */
class EcwidApiError extends \Exception {
}
