<?php declare(strict_types=1);

namespace Drupal\ecwid_drupal\Exception;

/**
 * A type error for the Ecwid store.
 *
 * For example, if the API returns some data with an unexpected type.
 */
class TypeError extends EcwidApiError {
}
