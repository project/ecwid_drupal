<?php declare(strict_types=1);

namespace Drupal\ecwid_drupal\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\MenuLinkManagerInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Link;
use Drupal\ecwid_drupal\EcwidApi;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Component\Datetime\TimeInterface;

class EcwidPageForm extends ContentEntityForm {
  /**
   * {@inheritDoc}
   */
  public function __construct(
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    TimeInterface $time,
    MessengerInterface $messenger,
    protected EcwidApi $ecwidApi,
    protected AliasManagerInterface $aliasManager,
    protected MenuLinkManagerInterface $menuLinkManager,
  ) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->setMessenger($messenger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('ecwid_drupal.ecwid_api'),
      $container->get('path_alias.manager'),
      $container->get('plugin.manager.menu.link'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    // Set the status to published (true) before saving.
    $this->entity->set('status', true);

    // Get the operation prior to saving.
    $operation = $this->entity->isNew() ? 'created' : 'updated';

    if ($this->entity->isNew()) {
      parent::save($form, $form_state);
    }

    // Check if the 'Provide a menu link' toggle is enabled.
    if ($form_state->getValue('menu_link_toggle')) {
      $parent = explode(':', $form_state->getValue('parent_menu_item'));
      // Prepare the menu link data.
      $menu_link_data = [
        'title' => $this->entity->label(),
        'menu_name' => $parent[0],
        // The first item in the string of menu items is the root menu name.
        'parent' => implode(':', array_slice($parent, 1)),
        'weight' => $form_state->getValue('weight'),
        'link' => ['uri' => 'entity:ecwid_page/' . $this->entity->id()],
      ];

      // Create or update the menu link.
      $this->updateOrCreateMenuLink($menu_link_data);
    }

    // @todo: this save is only performed so the pathauto alias is updated.
    parent::save($form, $form_state);

    if (!$form_state->getRedirect()) {
      // Redirect to the canonical route of the newly created/updated Ecwid Page.
      $form_state->setRedirect('entity.ecwid_page.canonical', [
        'ecwid_page' => $this->entity->id(),
      ]);
    }

    // Let the user know the operation was successful.
    $this->messenger()->addMessage(
      match ($operation) {
        'created' => $this->t(
          'The Ecwid Page %label was created successfully.',
          [
            '%label' => $this->entity->label(),
          ],
        ),
        'updated' => $this->t(
          'The Ecwid Page %label was updated successfully.',
          [
            '%label' => $this->entity->label(),
          ],
        ),
      },
    );
  }

  /**
   * Helper function to create or update a menu link.
   */
  private function updateOrCreateMenuLink(array $menu_link_data) {
    $existing_menu_link = $this->existingMenuLinks();
    // Load the menu link if it exists or create a new one.
    if (!empty($existing_menu_link)) {
      // Update existing menu link.
      $menu_link = reset($existing_menu_link);
      $this->menuLinkManager->updateDefinition(
        $menu_link->getPluginId(),
        $menu_link_data,
        true,
      );
    } else {
      // Create new menu link.
      \Drupal::entityTypeManager()
        ->getStorage('menu_link_content')
        ->create($menu_link_data)
        ->save();
    }
  }

  /**
   * Existing menu links for the EcwidPage entity (if any).
   *
   * @return \Drupal\Core\Menu\MenuLinkInterface[]
   */
  private function existingMenuLinks(): array {
    $url = $this->entity->toUrl();
    return $this->menuLinkManager->loadLinksByRoute(
      $url->getRouteName(),
      $url->getRouteParameters(),
    );
  }

  /**
   * Builds the entity form.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @return array
   *   The form structure.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $menu_links = $this->menuLinkManager->loadLinksByRoute(
      'entity:ecwid_page/*',
    );

    // Delete each menu link associated with the entity.
    foreach ($menu_links as $menu_link) {
      // @todo: The call to `delete()` below may be incorrect, should it be:
      // $this->menuLinkManager->removeDefinition($menu_link->getPluginId(), true);
      // I can't recall where I cribbed `delete()` from, and what the
      // intention is here.
      $menu_link->delete();
    }

    $form['#attached']['library'][] = 'ecwid_drupal/ecwid_page_form';
    $form = parent::buildForm($form, $form_state);

    if (!$this->ecwidApi->validateEcwidStore()) {
      $link = Link::createFromRoute(
        $this->t('Ecwid settings'),
        'ecwid_drupal.settings_form',
      )->toString();
      $this->messenger->addWarning(
        $this->t(
          'The Ecwid store is invalid. Please set or update your store id: @link.',
          [
            '@link' => $link,
          ],
        ),
      );
      $categories = [];
      $form['actions']['#disabled'] = true;
    } else {
      $categories = $this->ecwidApi->getCategories();
    }

    $options = [
      '' => $this->t(' - Select a category - '),
    ];
    foreach ($categories as $category) {
      $options[$category['id']] = $category['name'];
    }

    // Check if ecwid_category has a selected value
    $ecwidCategoryValue =
      $form_state->getValue('ecwid_category') ??
      $this->entity->ecwid_category->value;

    $form['ecwid_category'] = [
      '#type' => 'select',
      '#title' => $this->t('Ecwid Category'),
      '#options' => $options,
      '#default_value' => $ecwidCategoryValue ?? '',
      '#required' => true,
      '#ajax' => [
        'callback' => '::updateFormFields',
        'wrapper' => 'category-dependent-fields',
        'event' => 'change',
      ],
    ];

    // Container for fields that depend on ecwid_category
    $form['category_dependent_fields'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'category-dependent-fields'],
    ];

    $pathField = $form['path'];
    unset($form['path']);
    $titleField = $form['title'];
    unset($form['title']);

    if ($ecwidCategoryValue && is_numeric($ecwidCategoryValue)) {
      $category = $this->ecwidApi->getCategory((int) $ecwidCategoryValue);

      // Title field.
      $form['category_dependent_fields']['title'] = $titleField;

      // Catch a bug when editing an EcwidPage where the user has entered a
      // custom title.
      if (
        $titleField['widget'][0]['value']['#default_value'] !==
          $category['name'] &&
        !$this->entity->isNew()
      ) {
        $form['#attached']['drupalSettings']['ecwid_drupal'][
          'userEditedTitle'
        ] = true;
      }

      // Url alias field.
      $pathField['widget'][0]['alias']['#states'] += [
        'invisible' => [
          'input[name="path[0][path_auto]"]' => ['checked' => true],
        ],
      ];
      $form['category_dependent_fields']['path'] = $pathField;

      // Menu fields.
      /** @var \Drupal\Core\Menu\MenuParentFormSelectorInterface $menuParentSelector */
      $menuParentSelector = \Drupal::service('menu.parent_form_selector');

      $menuDefault = 'menu:';
      $menuPluginId = '';
      $menuWeight = $form_state->get('weight') ?? 0;
      if ($this->entity->id()) {
        $existingMenuLinks = $this->existingMenuLinks();
        $existingMenuLink = reset($existingMenuLinks);
        if (!empty($existingMenuLink)) {
          $menuDefault =
            $existingMenuLink->getMenuName() .
            ':' .
            $existingMenuLink->getParent();
          $menuPluginId = $existingMenuLink->getPluginId();
          $menuWeight = $existingMenuLink->getWeight();
        }
      }

      $parentElement = $menuParentSelector->parentSelectElement(
        $menuDefault,
        $menuPluginId,
        [
          'main' => $this->t('Main menu'),
        ],
      );

      // Add 'Provide a menu link' toggle.
      $form['category_dependent_fields']['menu_link_toggle'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Provide a menu link'),
        '#default_value' => 1,
      ];

      // Add form element for parent menu item select.
      $form['category_dependent_fields']['parent_menu_item'] = $parentElement;
      $form['category_dependent_fields']['parent_menu_item']['#states'][
        'visible'
      ][':input[name="menu_link_toggle"]'] = ['checked' => true];

      // Add field for menu item weight.
      $form['category_dependent_fields']['weight'] = [
        '#type' => 'number',
        '#title' => $this->t('Weight'),
        '#default_value' => $menuWeight,
        '#states' => [
          'visible' => [
            ':input[name="menu_link_toggle"]' => ['checked' => true],
          ],
        ],
      ];
    } else {
      // A category has not been selected in the drop-down.
      //
      // Title is a required field, so AJAX errors will occur if it's not
      // in the render array.
      $form['category_dependent_fields']['title'] = [
        ...$titleField,
        '#access' => false,
      ];
    }

    if ($this->entity->isNew()) {
      $form['actions']['save_create_another'] = [
        '#type' => 'submit',
        '#value' => $this->t('Save and create another'),
        '#submit' => ['::submitForm', '::saveAndCreateAnother'],
        '#weight' => 10,
        '#states' => [
          'disabled' => [
            ':input[name="ecwid_category"]' => ['value' => ''],
          ],
        ],
      ];
    }

    return $form;
  }

  /**
   * AJAX callback, which gets an updated form based on new state.
   */
  public function updateFormFields(
    array &$form,
    FormStateInterface $form_state,
  ) {
    return $form['category_dependent_fields'];
  }

  /**
   * Custom submission handler for the 'Save and create another' action.
   */
  public function saveAndCreateAnother(
    array &$form,
    FormStateInterface $form_state,
  ) {
    $this->save($form, $form_state);

    // Redirect the user back to the form to create another entity.
    $form_state->setRedirect('entity.ecwid_page.add_form');
  }
}
