<?php declare(strict_types=1);

namespace Drupal\ecwid_drupal\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Link;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\ecwid_drupal\EcwidApi;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Ecwid Store block.
 *
 * @Block(
 *   id = "ecwid_store",
 *   admin_label = @Translation("Ecwid Store Block"),
 *   category = @Translation("Ecwid integration")
 * )
 */
class StoreBlock extends BlockBase implements ContainerFactoryPluginInterface {
  /**
   * {@inheritDoc}
   */
  public function build(): array {
    $config = $this->getConfiguration();
    $storeConfig = $this->configFactory->get('ecwid_drupal.settings');
    $store_id = $storeConfig->get('store_id') ?? '';
    $category_id = $config['ecwid_category'] ?? '';
    $base_path = $storeConfig->get('store_base_path');

    if ($base_path === FALSE) {
      $link = Link::createFromRoute(
        $this->t('Ecwid settings'),
        'ecwid_drupal.settings_form',
      )->toString();

      $this->loggerChannelFactory->get('ecwid')->error(
        $this->t(
          'The Ecwid store base path is invalid. Please set or update your store base path (under Advanced): @link.',
          [
            '@link' => $link,
          ],
        ),
      );
    }

    try {
      $category = is_numeric($category_id)
        ? $this->EcwidApi->getCategory((int) $category_id)
        : NULL;
    }
    catch (ClientException $e) {
      $category = NULL;
      $printedError = print_r($e, TRUE);
      $this->loggerChannelFactory
        ->get('ecwid')
        ->error(
          'There was an error fetching the category for a store block. ' .
          'Displaying the store without a default category selected. ' .
          'Maybe the selected category has been deleted from the Ecwid ' .
          "store? The category id is '$category_id' and the error was: $printedError",
        );
    }

    return [
      '#theme' => 'ecwid_store_block',
      '#store_id' => $store_id,
      '#base_path' => $base_path,
      '#store_base_path' => $base_path,
      ...!is_null($category) ? ['#default_category' => $category] : [],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);
    $categories = $this->EcwidApi->getCategories();
    $options = ['' => 'Storefront (no category)'];

    foreach ($categories as $category) {
      $options[(string) $category['id']] = $category['name'];
    }

    $form['ecwid_category'] = [
      '#type' => 'select',
      '#title' => $this->t('Ecwid Category'),
      '#options' => $options,
      '#default_value' => $this->configuration['ecwid_category'] ?? '',
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['ecwid_category'] = $form_state->getValue('ecwid_category');
  }

  public function __construct(
    array $configuration,
    string $plugin_id,
    array $plugin_definition,
    protected readonly EcwidApi $EcwidApi,
    protected readonly ConfigFactoryInterface $configFactory,
    protected readonly LoggerChannelFactoryInterface $loggerChannelFactory,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('ecwid_drupal.ecwid_api'),
      $container->get('config.factory'),
      $container->get('logger.factory'),
    );
  }
}
