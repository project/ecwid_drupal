<?php declare(strict_types=1);

namespace Drupal\ecwid_drupal\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an Ecwid shopping cart block.
 *
 * @Block(
 *   id = "ecwid_cart",
 *   admin_label = @Translation("Ecwid Shopping Cart"),
 *   category = @Translation("Ecwid integration")
 * )
 */
class ShoppingCartBlock extends BlockBase {
  /**
   * {@inheritdoc}
   */
  public function build() {
    $store_id = \Drupal::config('ecwid_drupal.settings')->get('store_id');

    return [
      '#theme' => 'ecwid_cart_block',
      '#store_id' => $store_id,
    ];
  }
}
