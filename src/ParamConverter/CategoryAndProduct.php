<?php declare(strict_types=1);

namespace Drupal\ecwid_drupal\ParamConverter;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\ecwid_drupal\EcwidApi;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Route;

/**
 * {@inheritDoc}
 */
class CategoryAndProduct implements ParamConverterInterface {
  public function __construct(protected readonly EcwidApi $EcwidApi) {
  }

  /**
   * {@inheritDoc}
   */
  public function convert($value, $definition, $name, array $defaults): array {
    assert(
      is_numeric($value),
      new \TypeError(
        "Value for $name is not numeric. Value: '" .
          print_r($value, true) .
          "'",
      ),
    );

    try {
      if ($name === 'ecwid_category') {
        $convertedValue = $this->EcwidApi->getCategory((int) $value);
      } elseif ($name === 'ecwid_product') {
        // @todo: use the Ecwid API, once it supports V3.
        $convertedValue = ['id' => $value];
      } else {
        $exceptionName = print_r($name, true);
        $exceptionValue = print_r($value, true);
        throw new \ErrorException(
          'ParamConverter\CategoryAndProduct if statement did not cover every parameter name it might receive. This is a bug.' .
            " Parameter name: '$exceptionName'" .
            " Parameter value: '$exceptionValue'",
        );
      }
    } catch (ClientException $e) {
      // Ecwid API returned a 404 when the code above tried to retrieve
      // the category/product it.
      if ($e->getResponse()->getStatusCode() === 404) {
        $exceptionValue = print_r($value, true);
        throw new NotFoundHttpException("$name: $exceptionValue not found");
      } else {
        throw $e;
      }
    }

    return $convertedValue;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route): bool {
    // Determine if this converter should be applied.
    return $name === 'ecwid_category' || $name === 'ecwid_product';
  }
}
