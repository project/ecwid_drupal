<?php declare(strict_types=1);

namespace Drupal\ecwid_drupal\Controller;

use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\ecwid_drupal\EcwidApi;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Wrap `EcwidApi` functions.
 *
 * Turn specific errors into user messages and HTTP exceptions.
 */
trait EcwidApiTrait {
  use LoggerChannelTrait;
  use StringTranslationTrait;

  /**
   * The Ecwid API class allows us to check the user's connected store is valid.
   */
  protected readonly EcwidApi $ecwidApi;

  /**
   * Returns the current user.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   The current user.
   */
  abstract protected function currentUser();

  /**
   * Chainable function that throws if the store id is missing.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
   */
  public function ensureStoreId(): static {
    if (!$this->ecwidApi->validateEcwidStore()) {
      // If the user has permission to setup the store, give them a helpful
      // link in the event their store id has not been set.
      if ($this->currentUser()->hasPermission('administer ecwid_drupal')) {
        $link = Link::createFromRoute(
          $this->t('Ecwid settings'),
          'ecwid_drupal.settings_form',
        )->toString();

        $this->getLogger('ecwid')->critical(
          $this->t(
            'The Ecwid store is invalid. Please set or update your store id: @link.',
            [
              '@link' => $link,
            ],
          ),
        );
      }

      // Throw a 404 when the store id is not set.
      throw new NotFoundHttpException();
    }

    return $this;
  }
}
