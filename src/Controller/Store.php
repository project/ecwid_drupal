<?php declare(strict_types=1);

namespace Drupal\ecwid_drupal\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\ecwid_drupal\EcwidApi;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller that handles requests for Ecwid store URLs.
 */
class Store extends ControllerBase {
  use EcwidApiTrait;

  /**
   * Request for the store front, no category or product selected.
   */
  public function handleStoreRequest(Request $request) {
    $storeId = $this->ensureStoreId()
      ->config('ecwid_drupal.settings')
      ->get('store_id');

    return [
      '#theme' => 'ecwid_store_block',
      '#store_id' => $storeId,
      '#base_path' => $request->getPathInfo(),
    ];
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ecwid_drupal.ecwid_api'),
      $container->get('config.factory'),
      $container->get('logger.factory'),
    );
  }

  public function __construct(
    protected readonly EcwidApi $ecwidApi,
    ConfigFactoryInterface $configFactory,
    LoggerChannelFactoryInterface $loggerFactory,
  ) {
    $this->configFactory = $configFactory;
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * Respond to requests for internal Ecwid category URLs.
   *
   * For example:
   *  /product/some-category/SOME_ECWID_GENERATED_URL-c12345
   *   ^store base path ^Drupal path ^Ecwid path with category number.
   *
   * @see Drupal\ecwid_drupal\PathProcessor\StorePathNormaliser
   * @see Drupal\ecwid_drupal\Routing\RouteSubscriber
   */
  public function handleCategoryRequest(
    array $ecwid_category,
    Request $request,
  ): array {
    $storeId = $this->ensureStoreId()
      ->config('ecwid_drupal.settings')
      ->get('store_id');

    if (strpos($request->getPathInfo(), '/ecwid-store') === 0) {
      $this->loggerFactory
        ->get('ecwid')
        ->error(
          'Direct request to ecwid-store URI detected! These routes are for internal use only.' .
            PHP_EOL .
            print_r($request, true),
        );
    }

    return [
      '#theme' => 'ecwid_store_block',
      '#store_id' => $storeId,
      '#store_base_path' => $this->config('ecwid_drupal.settings')->get(
        'store_base_path',
      ),
      '#default_category' => $ecwid_category,
      '#base_path' => $request->getPathInfo(),
    ];
  }

  /**
   * Category title.
   */
  public function categoryTitle(array $ecwid_category) {
    return $ecwid_category['name'];
  }

  /**
   * Respond to requests for internal Ecwid product URLs.
   *
   * For example:
   *  /product/some-category/SOME_ECWID_GENERATED_URL-p12345
   *   ^store base path ^Drupal path ^Ecwid path with product number.
   *
   * @see Drupal\ecwid_drupal\PathProcessor\StorePathNormaliser
   * @see Drupal\ecwid_drupal\Routing\RouteSubscriber
   */
  public function handleProductRequest(
    array $ecwid_product,
    array|null $ecwid_category,
    Request $request,
  ): array {
    $storeId = $this->ensureStoreId()
      ->config('ecwid_drupal.settings')
      ->get('store_id');

    if (strpos($request->getPathInfo(), '/ecwid-store') === 0) {
      $this->loggerFactory
        ->get('ecwid')
        ->error(
          'Direct request to ecwid-store URI detected! These routes are for internal use only.' .
            PHP_EOL .
            print_r($request, true),
        );
    }

    // Get all but the product to use as the base_url.
    // @todo: This is a bit hacky, replace with something more formal once
    // an Ecwid Url class and parser are written.
    $basePath = implode(
      '/',
      array_slice(explode('/', $request->getPathInfo()), 0, -1),
    );

    $return = [
      '#theme' => 'ecwid_store_block',
      '#store_id' => $storeId,
      '#default_product' => $ecwid_product,
      '#base_path' => $basePath,
      ...empty($ecwid_category) ? [] : ['#default_category' => $ecwid_category],
    ];
    return $return;
  }
}
