<?php declare(strict_types=1);

namespace Drupal\ecwid_drupal\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\ecwid_drupal\EcwidApi;
use Drupal\ecwid_drupal\Entity\EcwidPage;
use Drupal\ecwid_drupal\Exception\TypeError;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\ecwid_drupal\Controller\EcwidApiTrait;

/**
 * Controller that handles requests for EcwidPage entities.
 */
class EcwidPageController extends ControllerBase {
  use EcwidApiTrait;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ecwid_drupal.ecwid_api'),
      $container->get('config.factory'),
      $container->get('logger.factory'),
    );
  }

  public function __construct(
    protected readonly EcwidApi $ecwidApi,
    ConfigFactoryInterface $configFactory,
    LoggerChannelFactoryInterface $loggerFactory,
  ) {
    $this->configFactory = $configFactory;
    $this->loggerFactory = $loggerFactory;
  }

  /**
   * View an EcwidPage entity.
   */
  public function view(EcwidPage $ecwid_page, Request $request): array {
    $storeId = $this->ensureStoreId()
      ->config('ecwid_drupal.settings')
      ->get('store_id');

    $category = $this->ecwidApi->getCategory(
      is_numeric($ecwid_page->ecwid_category->value)
        ? (int) $ecwid_page->ecwid_category->value
        : throw new TypeError(
          'Ecwid category of ' .
            $ecwid_page->ecwid_category->value .
            ' is not an integer',
        ),
    );

    return [
      '#theme' => 'ecwid_store_block',
      '#store_id' => $storeId,
      '#default_category' => $category,
      '#base_path' => $request->getPathInfo(),
    ];
  }

  /**
   * Title for an EcwidPage entity.
   */
  public function title(EcwidPage $ecwid_page): string {
    return $ecwid_page->title->value;
  }
}
